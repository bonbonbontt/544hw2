import random
import os
import shutil
from shutil import copy
import glob

def main():
	if os.path.isdir('./train/train_set'):
		shutil.rmtree('./train/train_set')
	if os.path.isdir('./train/test_set'):
		shutil.rmtree('./train/test_set')

	os.mkdir('./train/train_set')
	os.mkdir('./train/test_set')
	dialog_filenames = sorted(glob.glob(os.path.join('./train/train', "*.csv")))
	count_files = len(dialog_filenames)
	print(count_files)
	test_files, train_files = [],[]
	for i in range(int(count_files*0.25)):
		test_files.append(random.choice(dialog_filenames))
	train_files = list(set(dialog_filenames)-set(test_files))
	print("count train", len(train_files), "count test", len(test_files))
	print("copying test files")
	for file in test_files:
		_,tail = os.path.split(file)
		copy(file, os.path.join('./train/test_set',tail))
		print ('.',end='')

	print('')
	print("copying train files")
	for file in train_files:
		_,tail = os.path.split(file)
		copy(file, os.path.join('./train/train_set',tail))
		print ('.',end='')


if __name__ == '__main__':
	main()
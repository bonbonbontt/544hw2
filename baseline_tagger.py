from hw2_corpus_tool import get_utterances_from_file, get_utterances_from_filename, get_data
import pycrfsuite
import sys
import glob
import os
import random




def line2features(line):
	token, pos, features = [],[],[]
	label,speaker,tokens = line.act_tag, line.speaker, line.pos
	if tokens==None:
		return [],label, speaker
	for word in tokens:
		token.append('TOKEN_'+word.token)
		pos.append('POS_'+word.pos)
	features = {}
	# features.append(' '.join(token))
	# features.append(' '.join(pos))
	
	features = token+pos

	# features.append(token)
	# features.append(pos)
	return features,label, speaker

def dialog2features(dialog):
	feature_vector_list = []
	labels = []
	cur_speaker = ''
	for i in range(len(dialog)):
		features,label, speaker = line2features(dialog[i])
		if i==0:
			features.append("FIRST_UTTERANCE")
			# features['first'] = "FIRST_UTTERANCE"
			cur_speaker = speaker
		else:
			if speaker!=cur_speaker:
				features.append('CHANGE_SPEAKER')
				# features['change'] = 'CHANGE_SPEAKER'

		feature_vector_list.append(features)
		labels.append(label)

	return feature_vector_list, labels

def test():
	dialog = get_utterances_from_filename("./train/train/0001.csv")
	print(dialog[1])
	print(line2features(dialog[1]))
	x_train,y_train = dialog2features(dialog)

	trainer = pycrfsuite.Trainer(verbose=False)
	for xseq, yseq in zip(x_train, y_train):
		print(xseq, yseq)
		trainer.append([xseq], [yseq])
	trainer.set_params({
		'c1': 1.0,   # coefficient for L1 penalty
		'c2': 1e-3,  # coefficient for L2 penalty
		'max_iterations': 50,  # stop earlier
	
		# include transitions that are possible, but not observed
		'feature.possible_transitions': True
	})
	trainer.train('baseline.crfsuite')
	print("...training done...")
	print(trainer.logparser.last_iteration)

	tagger = pycrfsuite.Tagger()
	tagger.open('baseline.crfsuite')
	predict = tagger.tag([x_train[1]])
	print(x_train[1])
	print("predict",''.join(predict))
	print("real",y_train[1])






def main():
	# Read files
	inputdir, testdir, outputfile = sys.argv[1:]
	train_filenames = sorted(glob.glob(os.path.join(inputdir, "*.csv")))
	test_filenames = sorted(glob.glob(os.path.join(testdir, "*.csv")))
	
	# Train
	trainer = pycrfsuite.Trainer(verbose=False)
	for dialog_filename in train_filenames:
		dialog = get_utterances_from_filename(dialog_filename)
		x_train,y_train = dialog2features(dialog)
		for xseq, yseq in zip(x_train, y_train):
			trainer.append([xseq], [yseq])

	x_test, y_test = [],[]
	for dialog_filename in test_filenames:
		dialog = get_utterances_from_filename(dialog_filename)
		x,y = dialog2features(dialog)
		x_test.append(x)
		y_test.append(y)


	trainer.set_params({
		'c1': 1.0,   # coefficient for L1 penalty
		'c2': 1e-3,  # coefficient for L2 penalty
		'max_iterations': 50,  # stop earlier
	
		# include transitions that are possible, but not observed
		'feature.possible_transitions': False
	})
	trainer.train('baseline.crfsuite')

	print("...training done...")
	# print(trainer.logparser.last_iteration)
	
	# Make Predictions
	tagger = pycrfsuite.Tagger()
	tagger.open('baseline.crfsuite')
	y_pred = []
	for dialog in x_test:
		dialog_pred = [tagger.tag([xseq]) for xseq in dialog]
		y_pred.append(dialog_pred)
	print("...prediction done...")
	# y_pred = [tagger.tag([xseq]) for xseq in x_test]

	# Write to file
	f = open(outputfile,"w")
	for dialog in y_pred:
		for pred in dialog:
			f.write(''.join(pred)+'\n')
		f.write('\n')
	f.close()
	print("...writing to file done...")


	# Evaluate
	count_all, count_correct = 0.0,0.0
	for dialog_pred, dialog_real in zip(y_pred, y_test):
		for pred,real in zip(dialog_pred, dialog_real):
			# print (pred,real)
			if ''.join(pred)==real:
				count_correct+=1
			count_all+=1
	print("accuracy:", count_correct/count_all)







if __name__ == '__main__':
	main()


